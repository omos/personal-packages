# My personal RPM packages

Sources for my personal RPMs that I use to set up my work/personal
machines.

## Setup

```bash
sudo curl -L -o /etc/yum.repos.d/omos-personal-packages.repo https://omos.gitlab.io/personal-packages/omos-personal-packages.repo
sudo dnf install -y omos-...
```
