Name:           omos
Version:        1.0
Release:        %autorelease
Summary:        omos's personal packages

License:        MIT
URL:            https://gitlab.com/omos/personal-packages/

Source1:        omos-personal-packages.repo

BuildArch:      noarch

BuildRequires:  systemd-rpm-macros
BuildRequires:  mock-core-configs

%description
RPMs that I use to set up my work/personal machines.

%global omosconfigdir %{_datadir}/%{name}

%install
# Patched configs
install -d %{buildroot}%{omosconfigdir}

install -pm 0644 %{_sysconfdir}/mock/site-defaults.cfg %{buildroot}%{omosconfigdir}/mock-site-defaults.cfg.orig
install -pm 0644 %{_sysconfdir}/mock/site-defaults.cfg %{buildroot}%{omosconfigdir}/mock-site-defaults.cfg.patched
cat >>%{buildroot}%{omosconfigdir}/mock-site-defaults.cfg.patched <<EOF

# omos
config_opts['plugin_conf']['tmpfs_enable'] = True
config_opts['plugin_conf']['tmpfs_opts']['required_ram_mb'] = 4096
config_opts['plugin_conf']['tmpfs_opts']['keep_mounted'] = False
config_opts['plugin_conf']['ccache_enable'] = True
config_opts['plugin_conf']['ccache_opts']['max_cache_size'] = '4G'
EOF

# Config
install -d %{buildroot}%{_sysconfdir}/gdbinit.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/gdbinit.d/99_omos.gdb <<EOF
# set of essential int commands from msekleta
set pagination off
set confirm off
set print pretty on
set history save on
set history size 10000
set history filename ~/.gdb_history
EOF

install -d %{buildroot}%{_sysconfdir}/unbound/conf.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/unbound/conf.d/99_omos.conf <<EOF
server:
        # attempt to fix DNSSEC issues (taken from https://github.com/NLnetLabs/unbound/issues/88#issuecomment-536077420)
        #disable-dnssec-lame-check: yes
        # ^^ only helped a bit; at home in TN DNSSEC is just a nightmare, so make it permissive for now :(
        val-permissive-mode: yes
EOF

install -d %{buildroot}%{_sysconfdir}/NetworkManager/conf.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/NetworkManager/conf.d/99_omos.conf <<EOF
[main]
dns=none
EOF

install -d %{buildroot}%{_sysconfdir}/dnsmasq.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/dnsmasq.d/99_omos.conf <<EOF
# Otherwise DNS ain't working in VMs
cache-size=10000
EOF

install -d %{buildroot}%{_sysconfdir}/ssh/sshd_config.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/ssh/sshd_config.d/99-omos.conf <<EOF
PasswordAuthentication no

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS
EOF

# Systemd
install -d %{buildroot}%{_unitdir}/multi-user.target.wants
ln -s ../sshd.service %{buildroot}%{_unitdir}/multi-user.target.wants/sshd.service
ln -s ../unbound.service %{buildroot}%{_unitdir}/multi-user.target.wants/unbound.service
ln -s ../virtnetworkd.service %{buildroot}%{_unitdir}/multi-user.target.wants/virtnetworkd.service
install -d %{buildroot}%{_unitdir}/swap.target.wants
ln -s ../zram-swap.service %{buildroot}%{_unitdir}/swap.target.wants/zram-swap.service

# Repos
install -d %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 0644 %{SOURCE1} %{buildroot}%{_sysconfdir}/yum.repos.d/omos-personal-packages.repo
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/yum.repos.d/rcm-tools-fedora.repo <<EOF
[rcm-tools-fedora-rpms]
name=RCM Tools for Fedora \$releasever (RPMs)
baseurl=https://download.devel.redhat.com/rel-eng/RCMTOOLS/latest-RCMTOOLS-2-F-\$releasever/compose/Everything/\$basearch/os/
enabled=1
gpgcheck=1
gpgkey=https://download.devel.redhat.com/rel-eng/RCMTOOLS/RPM-GPG-KEY-rcminternal
skip_if_unavailable=True
EOF
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/yum.repos.d/qa-tools.repo <<EOF
[copr:copr.devel.redhat.com:lpol:qa-tools]
name=Copr repo for qa-tools owned by lpol
baseurl=http://coprbe.devel.redhat.com/results/lpol/qa-tools/fedora-\$releasever-\$basearch/
enabled=1
gpgcheck=1
gpgkey=http://coprbe.devel.redhat.com/results/lpol/qa-tools/pubkey.gpg
skip_if_unavailable=True
EOF
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/yum.repos.d/beaker-client.repo <<EOF
[beaker-client]
name=Beaker Client - Fedora\$releasever
baseurl=http://download.devel.redhat.com/beakerrepos/client/Fedora\$releasever/
enabled=1
gpgcheck=0
skip_if_unavailable=True

[beaker-client-testing]
name=Beaker Client - Fedora\$releasever - Testing
baseurl=http://download.devel.redhat.com/beakerrepos/client-testing/Fedora\$releasever/
enabled=0
gpgcheck=0
skip_if_unavailable=True
EOF
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/yum.repos.d/_copr\:copr.fedorainfracloud.org\:jbenc\:review-tools.repo <<EOF
[copr:copr.fedorainfracloud.org:jbenc:review-tools]
name=Copr repo for review-tools owned by jbenc
baseurl=https://download.copr.fedorainfracloud.org/results/jbenc/review-tools/fedora-\$releasever-\$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://download.copr.fedorainfracloud.org/results/jbenc/review-tools/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
EOF

# RH certs/kerberos
install -d %{buildroot}%{_sysconfdir}/pki/ca-trust/source/anchors
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/pki/ca-trust/source/anchors/Current-IT-Root-CAs.pem <<EOF
-----BEGIN CERTIFICATE-----
MIIENDCCAxygAwIBAgIJANunI0D662cnMA0GCSqGSIb3DQEBCwUAMIGlMQswCQYD
VQQGEwJVUzEXMBUGA1UECAwOTm9ydGggQ2Fyb2xpbmExEDAOBgNVBAcMB1JhbGVp
Z2gxFjAUBgNVBAoMDVJlZCBIYXQsIEluYy4xEzARBgNVBAsMClJlZCBIYXQgSVQx
GzAZBgNVBAMMElJlZCBIYXQgSVQgUm9vdCBDQTEhMB8GCSqGSIb3DQEJARYSaW5m
b3NlY0ByZWRoYXQuY29tMCAXDTE1MDcwNjE3MzgxMVoYDzIwNTUwNjI2MTczODEx
WjCBpTELMAkGA1UEBhMCVVMxFzAVBgNVBAgMDk5vcnRoIENhcm9saW5hMRAwDgYD
VQQHDAdSYWxlaWdoMRYwFAYDVQQKDA1SZWQgSGF0LCBJbmMuMRMwEQYDVQQLDApS
ZWQgSGF0IElUMRswGQYDVQQDDBJSZWQgSGF0IElUIFJvb3QgQ0ExITAfBgkqhkiG
9w0BCQEWEmluZm9zZWNAcmVkaGF0LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBALQt9OJQh6GC5LT1g80qNh0u50BQ4sZ/yZ8aETxt+5lnPVX6MHKz
bfwI6nO1aMG6j9bSw+6UUyPBHP796+FT/pTS+K0wsDV7c9XvHoxJBJJU38cdLkI2
c/i7lDqTfTcfLL2nyUBd2fQDk1B0fxrskhGIIZ3ifP1Ps4ltTkv8hRSob3VtNqSo
GxkKfvD2PKjTPxDPWYyruy9irLZioMffi3i/gCut0ZWtAyO3MVH5qWF/enKwgPES
X9po+TdCvRB/RUObBaM761EcrLSM1GqHNueSfqnho3AjLQ6dBnPWlo638Zm1VebK
BELyhkLWMSFkKwDmne0jQ02Y4g075vCKvCsCAwEAAaNjMGEwHQYDVR0OBBYEFH7R
4yC+UehIIPeuL8Zqw3PzbgcZMB8GA1UdIwQYMBaAFH7R4yC+UehIIPeuL8Zqw3Pz
bgcZMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgGGMA0GCSqGSIb3DQEB
CwUAA4IBAQBDNvD2Vm9sA5A9AlOJR8+en5Xz9hXcxJB5phxcZQ8jFoG04Vshvd0e
LEnUrMcfFgIZ4njMKTQCM4ZFUPAieyLx4f52HuDopp3e5JyIMfW+KFcNIpKwCsak
oSoKtIUOsUJK7qBVZxcrIyeQV2qcYOeZhtS5wBqIwOAhFwlCET7Ze58QHmS48slj
S9K0JAcps2xdnGu0fkzhSQxY8GPQNFTlr6rYld5+ID/hHeS76gq0YG3q6RLWRkHf
4eTkRjivAlExrFzKcljC4axKQlnOvVAzz+Gm32U0xPBF4ByePVxCJUHw1TsyTmel
RxNEp7yHoXcwn+fXna+t5JWh1gxUZty3
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIGcjCCBFqgAwIBAgIFICIEEFwwDQYJKoZIhvcNAQEMBQAwgaMxCzAJBgNVBAYT
AlVTMRcwFQYDVQQIDA5Ob3J0aCBDYXJvbGluYTEQMA4GA1UEBwwHUmFsZWlnaDEW
MBQGA1UECgwNUmVkIEhhdCwgSW5jLjETMBEGA1UECwwKUmVkIEhhdCBJVDEZMBcG
A1UEAwwQSW50ZXJuYWwgUm9vdCBDQTEhMB8GCSqGSIb3DQEJARYSaW5mb3NlY0By
ZWRoYXQuY29tMCAXDTIzMDQwNTE4MzM0NFoYDzIwNTIwNDAyMTgzMzQ0WjCBozEL
MAkGA1UEBhMCVVMxFzAVBgNVBAgMDk5vcnRoIENhcm9saW5hMRAwDgYDVQQHDAdS
YWxlaWdoMRYwFAYDVQQKDA1SZWQgSGF0LCBJbmMuMRMwEQYDVQQLDApSZWQgSGF0
IElUMRkwFwYDVQQDDBBJbnRlcm5hbCBSb290IENBMSEwHwYJKoZIhvcNAQkBFhJp
bmZvc2VjQHJlZGhhdC5jb20wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoIC
AQCxuloEVglzWXZ9FFFUOSVdpRIB2jW5YBpwgMem2fPZeWIIvrVQ6PL9XNenDOXu
BHbShD/PApxi/ujSZyOIjLsNh7WDO+0NqpkfTyB9wUYAhx3GTIGY75RSoyZy1yKb
ZDTKv+rSfui9IlstAMz6L3OQLZES9zAYK8ICiDUwTeNZ7quA6qf0Kam2LyuBc/bl
BI7WFLOGGWY135P1OUXJgnJUsMhnYMTgvZQyJ2P7eLQpiR8TOr5ZI6CYapiyG64L
nkr/rsALjSxoUo09Yai1CVO66VFJ/XgMNt3mzQtLDMPXiKUuwsBsgvo4QvLjkXYI
ii+/YQyQaypsKctG8mefKkTT1kRDKj4LNdTRRgd5tco+b4+O/4upt8mIsx1+tbdM
LNGEz3Jqd0sj8Fl4Rzus+W+enzXmMfZH86X6bU5tMvueuFd5LV+M9XzliscaEQMK
EQ7CC72ldrOK2K12Gjb7bu8dKq+aSlNuWK+Gz1NvbwYpaCBYp0JoryvHEq5jrCLP
lTkuJQ3HaaAf+4LaBm8no9xK2VbDf6l/7Htb5I5LnAAZi0/5TzH07NhHoIeMSmTE
Ea07i/i5lbhM2qbx6pfLukg24HLCKTdi4Fo6/JqPWH6/3eI55NsoWSmoDdTiLg4v
1G/rgUVr2N6F36GTYMGqiITvvd4Qm3i9XOTQvsx8RJx4JQIDAQABo4GoMIGlMB0G
A1UdDgQWBBS1+o3lCnihCZXbTSGGlWpZT0nIizAfBgNVHSMEGDAWgBS1+o3lCnih
CZXbTSGGlWpZT0nIizAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAR
BglghkgBhvhCAQEEBAMCAQYwLwYDVR0fBCgwJjAkoCKgIIYeaHR0cDovL29jc3Au
cmVkaGF0LmNvbS9jcmwucGVtMA0GCSqGSIb3DQEBDAUAA4ICAQCDLaGTS0g2HmMS
g0i6Z0RVDC7sSnWFgEk2ZO1WUQj5WkFVS7gWxed/mXCzeL2EV1Pd22YKHM1eU1vo
6b03cbNRXlRGGFksmQeM9h2sVjbP0hRZxqqfI+UW223N8E+qK3wSa8m6nhOfIJie
DD9s8CdL1VT6l4qq2gR8mVBW7EZ+Ux5u+AMXpN4WPEkcLer2djbfhXoPsJ4r5CcX
vh7W5rCZbo+0oBI5hrTlG4Tjhv1atqLhMmssjn8NbRrnhrbGF7w8NxFts69GkKDB
UIXr1pWZSAuRELlIxmvh5ZSX5YTbFmDuTvmNx8RPPy6OY4W1v1BUKp0HyJTi07s2
8SN+n9htHPHX9XBZctQmOSFLiqhi15LIqI54tR2tSgwH3Z5moh4sy6MuApXstsu4
qtkII2KZk3SottI8MOS6zqKrU7jPou6ZE0fznNiu23Q3Ksuuj6mBkLVw3bQe68Vm
NUTDac1oVzc8d5NMbx5kVb4Lahq+SATVFC8NK9G/Pk1AiwO8WhKffySsLeO5nMib
4BOVq0qFoAi8YCFuJOl9FlH1dPW/TnqlTQMQNhXpzGjU3HV3lr/Mk+ghNgIYcLcz
pEBsiGwKOVW4nYKIqPLn/36Ao/kfXeAdJhaAZq1SkTbeqNiwHQm3KNHzNObmjD0f
56vmq8fwQYIcazjrygWiaOnoep/SMw==
-----END CERTIFICATE-----
EOF

install -d %{buildroot}%{_sysconfdir}/krb5.conf.d
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/krb5.conf.d/ipa_redhat_com <<EOF
[libdefaults]
 default_realm = IPA.REDHAT.COM

[realms]
  IPA.REDHAT.COM = {
    pkinit_anchors = FILE:/etc/ipa/ca.crt
    pkinit_pool = FILE:/etc/ipa/ca.crt
    #default_domain = ipa.redhat.com
    dns_lookup_kdc = true
    # Trust tickets issued by legacy realm on this host
    auth_to_local = RULE:[1:\$1@\$0](.*@REDHAT\\.COM)s/@.*//
    auth_to_local = DEFAULT
  }

[domain_realm]
  kojihub.stream.centos.org = REDHAT.COM
EOF

install -d %{buildroot}%{_sysconfdir}/ipa
install -pm 0644 /dev/stdin %{buildroot}%{_sysconfdir}/ipa/ca.crt <<EOF
-----BEGIN CERTIFICATE-----
MIID6DCCAtCgAwIBAgIBFDANBgkqhkiG9w0BAQsFADCBpTELMAkGA1UEBhMCVVMx
FzAVBgNVBAgMDk5vcnRoIENhcm9saW5hMRAwDgYDVQQHDAdSYWxlaWdoMRYwFAYD
VQQKDA1SZWQgSGF0LCBJbmMuMRMwEQYDVQQLDApSZWQgSGF0IElUMRswGQYDVQQD
DBJSZWQgSGF0IElUIFJvb3QgQ0ExITAfBgkqhkiG9w0BCQEWEmluZm9zZWNAcmVk
aGF0LmNvbTAeFw0xNTEwMTQxNzI5MDdaFw00NTEwMDYxNzI5MDdaME4xEDAOBgNV
BAoMB1JlZCBIYXQxDTALBgNVBAsMBHByb2QxKzApBgNVBAMMIkludGVybWVkaWF0
ZSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
ggEKAoIBAQDYpVfg+jjQ3546GHF6sxwMOjIwpOmgAXiHS4pgaCmu+AQwBs4rwxvF
S+SsDHDTVDvpxJYBwJ6h8S3LK9xk70yGsOAu30EqITj6T+ZPbJG6C/0I5ukEVIeA
xkgPeCBYiiPwoNc/te6Ry2wlaeH9iTVX8fx32xroSkl65P59/dMttrQtSuQX8jLS
5rBSjBfILSsaUywND319E/Gkqvh6lo3TEax9rhqbNh2s+26AfBJoukZstg3TWlI/
pi8v/D3ZFDDEIOXrP0JEfe8ETmm87T1CPdPIZ9+/c4ADPHjdmeBAJddmT0IsH9e6
Gea2R/fQaSrIQPVmm/0QX2wlY4JfxyLJAgMBAAGjeTB3MB0GA1UdDgQWBBQw3gRU
oYYCnxH6UPkFcKcowMBP/DAfBgNVHSMEGDAWgBR+0eMgvlHoSCD3ri/GasNz824H
GTASBgNVHRMBAf8ECDAGAQH/AgEBMA4GA1UdDwEB/wQEAwIBhjARBglghkgBhvhC
AQEEBAMCAQYwDQYJKoZIhvcNAQELBQADggEBADwaXLIOqoyQoBVck8/52AjWw1Cv
ath9NGUEFROYm15VbAaFmeY2oQ0EV3tQRm32C9qe9RxVU8DBDjBuNyYhLg3k6/1Z
JXggtSMtffr5T83bxgfh+vNxF7o5oNxEgRUYTBi4aV7v9LiDd1b7YAsUwj4NPWYZ
dbuypFSWCoV7ReNt+37muMEZwi+yGIU9ug8hLOrvriEdU3RXt5XNISMMuC8JULdE
3GVzoNtkznqv5ySEj4M9WsdBiG6bm4aBYIOE0XKE6QYtlsjTMB9UTXxmlUvDE0wC
z9YYKfC1vLxL2wAgMhOCdKZM+Qlu1stb0B/EF3oxc/iZrhDvJLjijbMpphw=
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIENDCCAxygAwIBAgIJANunI0D662cnMA0GCSqGSIb3DQEBCwUAMIGlMQswCQYD
VQQGEwJVUzEXMBUGA1UECAwOTm9ydGggQ2Fyb2xpbmExEDAOBgNVBAcMB1JhbGVp
Z2gxFjAUBgNVBAoMDVJlZCBIYXQsIEluYy4xEzARBgNVBAsMClJlZCBIYXQgSVQx
GzAZBgNVBAMMElJlZCBIYXQgSVQgUm9vdCBDQTEhMB8GCSqGSIb3DQEJARYSaW5m
b3NlY0ByZWRoYXQuY29tMCAXDTE1MDcwNjE3MzgxMVoYDzIwNTUwNjI2MTczODEx
WjCBpTELMAkGA1UEBhMCVVMxFzAVBgNVBAgMDk5vcnRoIENhcm9saW5hMRAwDgYD
VQQHDAdSYWxlaWdoMRYwFAYDVQQKDA1SZWQgSGF0LCBJbmMuMRMwEQYDVQQLDApS
ZWQgSGF0IElUMRswGQYDVQQDDBJSZWQgSGF0IElUIFJvb3QgQ0ExITAfBgkqhkiG
9w0BCQEWEmluZm9zZWNAcmVkaGF0LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEP
ADCCAQoCggEBALQt9OJQh6GC5LT1g80qNh0u50BQ4sZ/yZ8aETxt+5lnPVX6MHKz
bfwI6nO1aMG6j9bSw+6UUyPBHP796+FT/pTS+K0wsDV7c9XvHoxJBJJU38cdLkI2
c/i7lDqTfTcfLL2nyUBd2fQDk1B0fxrskhGIIZ3ifP1Ps4ltTkv8hRSob3VtNqSo
GxkKfvD2PKjTPxDPWYyruy9irLZioMffi3i/gCut0ZWtAyO3MVH5qWF/enKwgPES
X9po+TdCvRB/RUObBaM761EcrLSM1GqHNueSfqnho3AjLQ6dBnPWlo638Zm1VebK
BELyhkLWMSFkKwDmne0jQ02Y4g075vCKvCsCAwEAAaNjMGEwHQYDVR0OBBYEFH7R
4yC+UehIIPeuL8Zqw3PzbgcZMB8GA1UdIwQYMBaAFH7R4yC+UehIIPeuL8Zqw3Pz
bgcZMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgGGMA0GCSqGSIb3DQEB
CwUAA4IBAQBDNvD2Vm9sA5A9AlOJR8+en5Xz9hXcxJB5phxcZQ8jFoG04Vshvd0e
LEnUrMcfFgIZ4njMKTQCM4ZFUPAieyLx4f52HuDopp3e5JyIMfW+KFcNIpKwCsak
oSoKtIUOsUJK7qBVZxcrIyeQV2qcYOeZhtS5wBqIwOAhFwlCET7Ze58QHmS48slj
S9K0JAcps2xdnGu0fkzhSQxY8GPQNFTlr6rYld5+ID/hHeS76gq0YG3q6RLWRkHf
4eTkRjivAlExrFzKcljC4axKQlnOvVAzz+Gm32U0xPBF4ByePVxCJUHw1TsyTmel
RxNEp7yHoXcwn+fXna+t5JWh1gxUZty3
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIEqjCCA5KgAwIBAgIBGTANBgkqhkiG9w0BAQsFADBOMRAwDgYDVQQKDAdSZWQg
SGF0MQ0wCwYDVQQLDARwcm9kMSswKQYDVQQDDCJJbnRlcm1lZGlhdGUgQ2VydGlm
aWNhdGUgQXV0aG9yaXR5MB4XDTE3MTAxNjE5MTk1N1oXDTM3MTAxMTE5MTk1N1ow
OTEXMBUGA1UECgwOSVBBLlJFREhBVC5DT00xHjAcBgNVBAMMFUNlcnRpZmljYXRl
IEF1dGhvcml0eTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANOwt6Et
NANZO9uIOmRQE61277K5/jtioUOpVJrrIQEmz9MPufVfisKbl1eUolxDJh6/Q7ZQ
XxYHHEJtFPKA5n73U3JFuHOjcxZHxrEBvTMiKzlG5RNLSQqE0m0DgFq20tNay17W
oZJCSzBcXqJ9mW37+Iuy4vvEhYuo4LtGIQVBNY+Vei5h2Ihsqrnc7iJCXA5N8Wix
ErmCIvRSxz5NR4z9WZEyVAwhzmzKOrzZHvB8iFP9Q76AersOD6CLGlKCcwAc4myq
Ef9yK5oo4NppFFntX8X+as+SHKi1LboqnsTdwfdJKYzPxdKUHKcmGXlo1zmD+zm9
gN8BilZ7G/7xHPX0yuWCBA06pBhDAjY4RFl6Atu/LdcIbaRXEqvMUCrDuIxMgfXy
FFoG4YyuZ3VhSQMM5NNN4BqdLiXO0QaxPUFXDvwPXj+ISPohROXleJFKbhUUUpXm
NqmGOC+IKVotO58yFx2/cy1CtsTUkS2Z55nEKvzgrAzg5Sy54Togs5an4i5NiMpP
QPaM6V/pzri7dQKeo2lhffTTW3KuHzipCWNWNP68EMExK3Qcg82vNdcf+F2PuYVZ
IaNmDbkgt5XOWs/jUF1nohiuro6DYpsFue+S84vQwJphPJLMJt4YKUdpJwdFCtVE
0wyYuOSXKVSKlcizVbnzU2EVZXv0wRK8s1/rAgMBAAGjgacwgaQwHwYDVR0jBBgw
FoAUMN4EVKGGAp8R+lD5BXCnKMDAT/wwHQYDVR0OBBYEFGRVQZQp8pHtsV0ZKIt6
su2hDwxIMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYDVR0PAQH/BAQDAgHGMD4GCCsG
AQUFBwEBBDIwMDAuBggrBgEFBQcwAYYiaHR0cDovL29jc3AucmVkaGF0LmNvbS9j
YS9pbnRvY3NwLzANBgkqhkiG9w0BAQsFAAOCAQEA1bfh/GbCd0QaJTzH+nuOWHVq
JLhDg/vFynidq+mFQUKQXgzYmcKVciIFHuk8yui97dVX2bJ0mAwYhvTKIEH1Cpmm
0DM4M/kMEoLANK1vCRmschwWNiAFmQkUo6lAHLDImYTA0XEZshHqhUvCLb+uKYV5
Sbl3ZZ2xm5rO54VnYTCBSYAzKd8i2Y1Vbe5BrirCYlxOGtE1b/5/cVyu8y6JrThq
zU+Ii9Asii6XJ3o1RElm65SPz3wqc3wowCyJMgJsrNeScQNeQKzBYRxuRkdNc/Wv
gC12EYMDVgGQgXX/d8LUIY7YHVVGO7B5AhMiboyFjqCLIgJz1y5Bhz9JyTHnNw==
-----END CERTIFICATE-----
EOF

# RH VPN
install -d %{buildroot}%{_sysconfdir}/NetworkManager/system-connections
install -pm 0600 /dev/stdin %{buildroot}%{_sysconfdir}/NetworkManager/system-connections/ovpn-brq-udp-x86-64.nmconnection <<EOF
[connection]
id=ovpn-brq-udp-x86-64
uuid=521fde7d-7eef-4bb2-97d4-f4fcc175481e
type=vpn

[vpn]
ca=/etc/pki/ca-trust/source/anchors/Current-IT-Root-CAs.pem
cipher=AES-256-CBC
connection-type=password
password-flags=1
port=443
remote=ovpn-brq.redhat.com
reneg-seconds=0
tunnel-mtu=1360
username=omosnace
service-type=org.freedesktop.NetworkManager.openvpn

[ipv4]
method=auto
never-default=true

[ipv6]
addr-gen-mode=stable-privacy
method=auto
never-default=true

[proxy]
EOF

# platform-python symlink for RHEL 8 kernel builds
install -d %{buildroot}%{_libexecdir}
ln -s %{_bindir}/python3 %{buildroot}%{_libexecdir}/platform-python


%package unwanted-packages-common
Summary:        omos's unwanted packages blocker

Conflicts:      tracker
Conflicts:      tracker-miners
Conflicts:      cryfs
Conflicts:      deltarpm
Conflicts:      clamav-data
Conflicts:      qt6-doc

%if 0%{?fedora} >= 40
Requires:       kwin-x11 plasma-workspace-x11
%endif

# pulled in by tox:
%if 0%{?fedora} != 26 && 0%{?fedora} != 27 && 0%{?fedora} != 28
Conflicts:      python3.6
%endif
%if 0%{?fedora} != 29 && 0%{?fedora} != 30 && 0%{?fedora} != 31
Conflicts:      python3.7
%endif
%if 0%{?fedora} != 32
Conflicts:      python3.8
%endif
%if 0%{?fedora} != 33 && 0%{?fedora} != 34
Conflicts:      python3.9
Conflicts:      pypy3.9
%endif
%if 0%{?fedora} != 35 && 0%{?fedora} != 36
Conflicts:      python3.10
%endif
%if 0%{?fedora} != 37 && 0%{?fedora} != 38
Conflicts:      python3.11
%endif
%if 0%{?fedora} < 39
Conflicts:      python3.12
%endif

Conflicts:      proj-data-at
Conflicts:      proj-data-au
Conflicts:      proj-data-be
Conflicts:      proj-data-br
Conflicts:      proj-data-ca
Conflicts:      proj-data-ch
Conflicts:      proj-data-de
Conflicts:      proj-data-dk
Conflicts:      proj-data-es
Conflicts:      proj-data-eur
Conflicts:      proj-data-fi
Conflicts:      proj-data-fo
Conflicts:      proj-data-fr
Conflicts:      proj-data-is
Conflicts:      proj-data-jp
Conflicts:      proj-data-mx
Conflicts:      proj-data-nc
Conflicts:      proj-data-nl
Conflicts:      proj-data-no
Conflicts:      proj-data-nz
Conflicts:      proj-data-pt
Conflicts:      proj-data-se
Conflicts:      proj-data-sk
Conflicts:      proj-data-uk
Conflicts:      proj-data-us
Conflicts:      proj-data-za

Conflicts:      podman container-selinux
Conflicts:      python3-botocore
Conflicts:      NetworkManager-libreswan-gnome
Conflicts:      ansible-core

Obsoletes:      kwrite

# These all break TrackMania Nations Forever :(
Obsoletes:      wine-core = 8.5-1%{?dist}
Obsoletes:      wine-core = 8.6-1%{?dist}
Obsoletes:      wine-core = 8.11-1%{?dist}

# https://gitlab.com/gnuwget/wget2/-/issues/689
Obsoletes:      wget2 = 2.2.0-1%{?dist}
Obsoletes:      wget2-libs = 2.2.0-1%{?dist}
Obsoletes:      wget2-wget = 2.2.0-1%{?dist}

# This version seems broken
Obsoletes:      firefox           = 120.0-2%{?dist}
Obsoletes:      firefox-langpacks = 120.0-2%{?dist}

# crashes and breaks often
Obsoletes:      kwin            = 6.3.0-1%{?dist}
Obsoletes:      kwin-x11        = 6.3.0-1%{?dist}
Obsoletes:      kwin-common     = 6.3.0-1%{?dist}
Obsoletes:      kwin-libs       = 6.3.0-1%{?dist}
Obsoletes:      kwin-wayland    = 6.3.0-1%{?dist}
Obsoletes:      kde-gtk-config  = 6.3.0-1%{?dist}
Obsoletes:      kdecoration     = 6.3.0-1%{?dist}
Obsoletes:      plasma-breeze   = 6.3.0-1%{?dist}

Obsoletes:      f29-backgrounds-base f29-backgrounds-kde
Obsoletes:      f30-backgrounds-base f30-backgrounds-kde
Obsoletes:      f31-backgrounds-base f31-backgrounds-kde
Obsoletes:      f32-backgrounds-base f32-backgrounds-kde
Obsoletes:      f33-backgrounds-base f33-backgrounds-kde
Obsoletes:      f34-backgrounds-base f34-backgrounds-kde
Obsoletes:      f35-backgrounds-base f35-backgrounds-kde
%if 0%{?fedora} >= 37
Obsoletes:      f36-backgrounds-base f36-backgrounds-kde
%endif
%if 0%{?fedora} >= 38
Obsoletes:      f37-backgrounds-base f37-backgrounds-kde
%endif
%if 0%{?fedora} >= 39
Obsoletes:      f38-backgrounds-base f38-backgrounds-kde
%endif
%if 0%{?fedora} >= 40
Obsoletes:      f39-backgrounds-base f39-backgrounds-kde
%endif
%if 0%{?fedora} >= 41
Obsoletes:      f40-backgrounds-base f40-backgrounds-kde
%endif

Obsoletes:      llvm7.0-libs
Obsoletes:      llvm8.0-libs
Obsoletes:      llvm10-libs
Obsoletes:      llvm11-libs
Obsoletes:      llvm12-libs
Obsoletes:      llvm13-libs
Obsoletes:      llvm14-libs
Obsoletes:      llvm15-libs
Obsoletes:      llvm16-libs
%if 0%{?fedora} >= 41
Obsoletes:      llvm17-libs clang17-libs
%endif

%description unwanted-packages-common
A simple personal RPM package to block unwanted weak dependencies from
being installed.

%files unwanted-packages-common


%package unwanted-packages-intel-gpu
Summary:        omos's unwanted packages blocker (Intel GPU)

Requires:       %{name}-unwanted-packages-common = %{version}-%{release}
%if 0%{?fedora} >= 37
Conflicts:      amd-gpu-firmware nvidia-gpu-firmware
Obsoletes:      amd-gpu-firmware nvidia-gpu-firmware
%endif

%description unwanted-packages-intel-gpu
A simple personal RPM package to block unwanted weak dependencies from
being installed (Intel GPU).

%files unwanted-packages-intel-gpu


%package machine-lenovo-t14s
Summary:        omos's config for Lenovo T14s machines

Requires:       %{name}-unwanted-packages-intel-gpu = %{version}-%{release}

Conflicts:      atmel-firmware zd1211-firmware
Obsoletes:      atmel-firmware zd1211-firmware

# loads iwlmvm, thus likely only needs iwlwifi-mvm-firmware
Conflicts:      iwlegacy-firmware iwlwifi-dvm-firmware
Obsoletes:      iwlegacy-firmware iwlwifi-dvm-firmware

%if 0%{?fedora} >= 39
Conflicts:      atheros-firmware brcmfmac-firmware mt7xxx-firmware libertas-firmware
Obsoletes:      atheros-firmware brcmfmac-firmware mt7xxx-firmware libertas-firmware
%else
Conflicts:      libertas-usb8388-firmware
Obsoletes:      libertas-usb8388-firmware
%endif

%if 0%{?fedora} >= 40
Conflicts:      amd-ucode-firmware
Obsoletes:      amd-ucode-firmware
Conflicts:      cirrus-audio-firmware
Obsoletes:      cirrus-audio-firmware
Conflicts:      nxpwireless-firmware
Obsoletes:      nxpwireless-firmware
Conflicts:      tiwilink-firmware
Obsoletes:      tiwilink-firmware
%endif

%description machine-lenovo-t14s
omos's config for Lenovo T14s machines.

%files machine-lenovo-t14s


%package config-common
Summary:        omos's common config RPM

# System packages
Requires:       unbound dnssec-trigger
Conflicts:      systemd-resolved
Obsoletes:      systemd-resolved
Requires:       irqbalance
Requires:       policycoreutils-restorecond
Requires:       zram

# Essential devel packages
Requires:       gcc gcc-c++ clang
Requires:       gdb
Requires:       make
Requires:       cmake
Requires:       meson
Requires:       libtool automake autoconf
Requires:       bison flex
Requires:       ccache
Requires:       git git-delta git-email
Requires:       valgrind
Requires:       vim-enhanced

# GUI devel packages
Requires:       qt-creator
Requires:       kate
Requires:       gitg gitk
Requires:       gparted
Requires:       sqlitebrowser
Requires:       kcachegrind
Requires:       meld

# Advanced devel packages
Requires:       astyle perltidy
Requires:       coreos-installer
Requires:       cppcheck
Requires:       cppunit-devel
#Requires:       did
Requires:       gh
Requires:       ignition-validate
Requires:       jq
Requires:       libkcapi-tools libkcapi-tests
Requires:       ltrace strace fatrace
Requires:       opencl-headers ocl-icd clinfo
Requires:       python3-pip
Requires:       qt5-qtbase-devel qt6-qtbase-devel
Requires:       selint secilc setools-console selinux-policy-devel
Requires:       sox
Requires:       xmlto
Requires:       yamllint
Requires:       zram-generator

# Python devel packages
Requires:       tox
Requires:       poetry

# Kernel devel packages
Requires:       coccinelle sparse
Requires:       elfutils-libelf-devel openssl-devel
Requires:       dwarves
Requires:       busybox
Requires:       qemu-system-x86
Requires:       virtme-ng

# Fedora devel packages
Requires:       fedora-packager fedora-review
Requires:       rust2rpm
Requires:       fbrnch
Requires:       fedrq
Requires:       tmt
Requires:       createrepo_c

# Browsers
Requires:       firefox
#Recommends:     chromium-freeworld

# Multimedia
Requires:       vlc
Requires:       ffmpeg
Requires:       mplayer
Requires:       digikam hugin
Requires:       gimp
Requires:       fatsort
Requires:       picard
Requires:       rsgain
Requires:       strawberry
# Better than youtube-dl
Requires:       yt-dlp

# CLI utils
Requires:       bash-completion
Requires:       htop
Requires:       lshw
Requires:       nmap
Requires:       openssh-clients
Requires:       p7zip
Requires:       pdftk
Requires:       redshift
Requires:       rsync
Requires:       smartmontools
Requires:       wget
Requires:       which
Requires:       xclip

# GUI utils
Requires:       k4dirstat
Requires:       kgraphviewer
Requires:       kgpg
%if 0%{?fedora} < 40
Requires:       ksysguard
%endif
Requires:       qbittorrent
#Requires:       qgis
Requires:       inkscape
Requires:       libreoffice
Requires:       setroubleshoot
Requires:       tigervnc
Requires:       wine
Requires:       wireshark

# Games:
Requires:       minetest
Requires:       openttd
Requires:       chessx stockfish
Requires:       stellarium
Requires:       steam

# Misc
Requires:       hunspell-sk hyphen-sk
Requires:       hunspell-cs hyphen-cs
Requires:       hunspell-en hyphen-en

# qemu-user-static (useful e.g. with mock)
# make sure only the Fedora-supported arches are pulled
Conflicts:      qemu-user-static
Requires:       qemu-user-static-x86
Requires:       qemu-user-static-aarch64
Requires:       qemu-user-static-ppc
Requires:       qemu-user-static-s390x

%description config-common
omos's common configuration and packages.

%post config-common
cp %{omosconfigdir}/mock-site-defaults.cfg.patched %{_sysconfdir}/mock/site-defaults.cfg

%preun config-common
if [ $1 -eq 0 ]; then
  cp %{omosconfigdir}/mock-site-defaults.cfg.orig %{_sysconfdir}/mock/site-defaults.cfg
fi

%files config-common
%{_sysconfdir}/gdbinit.d/99_omos.gdb
%{_sysconfdir}/NetworkManager/conf.d/99_omos.conf
%{_sysconfdir}/unbound/conf.d/99_omos.conf
%{_unitdir}/multi-user.target.wants/unbound.service
%{_unitdir}/swap.target.wants/zram-swap.service
%{omosconfigdir}/mock-site-defaults.cfg.orig
%{omosconfigdir}/mock-site-defaults.cfg.patched


%package config-vms
Summary:        omos's config RPM for virtual machine support

Requires:       virt-manager virt-install
Requires:       libvirt-daemon libvirt-client
Requires:       guestfs-tools libguestfs-ufs
Requires:       qemu-system-x86
Requires:       genisoimage

%description config-vms
omos's config RPM for virtual machine support.

%files config-vms
%{_sysconfdir}/dnsmasq.d/99_omos.conf
%{_unitdir}/multi-user.target.wants/virtnetworkd.service


%package config-ssh
Summary:        omos's config RPM for SSH daemon

Requires:       openssh-server

%description config-ssh
omos's config RPM for SSH daemon.

%files config-ssh
%{_sysconfdir}/ssh/sshd_config.d/99-omos.conf
%{_unitdir}/multi-user.target.wants/sshd.service


%package repos-common
Summary:        omos's package with extra repos

Requires:       rpmfusion-free-release
Requires:       rpmfusion-nonfree-release

%description repos-common
omos's package with extra repos.

%files repos-common
%{_sysconfdir}/yum.repos.d/omos-personal-packages.repo


%package repos-redhat
Summary:        omos's package with internal RH repos

Requires:       omos-repos-common

%description repos-redhat
omos's package with internal RH repos.

%files repos-redhat
%{_sysconfdir}/yum.repos.d/beaker-client.repo
%{_sysconfdir}/yum.repos.d/qa-tools.repo
%{_sysconfdir}/yum.repos.d/rcm-tools-fedora.repo
%{_sysconfdir}/yum.repos.d/_copr:copr.fedorainfracloud.org:jbenc:review-tools.repo


%package redhat-ca
Summary:        omos's package with internal RH root certificate

%description redhat-ca
omos's package with internal RH root certificate.

%post redhat-ca
update-ca-trust

%files redhat-ca
%{_sysconfdir}/pki/ca-trust/source/anchors/Current-IT-Root-CAs.pem


%package redhat-vpn
Summary:        omos's package with internal RH VPN configuration

Requires:       omos-redhat-ca

Requires:       NetworkManager

%description redhat-vpn
omos's package with internal VPN configuration.

%files redhat-vpn
%{_sysconfdir}/NetworkManager/system-connections/ovpn-brq-udp-x86-64.nmconnection


%package redhat-printers
Summary:        omos's package with internal RH printer configuration

Requires(post):  cups
Requires:        cups

%description redhat-printers
omos's package with internal RH printer configuration.

%post redhat-printers
lpadmin -p RedHat_SecurePrint \
  -v ipps://fra-rps02.win.redhat.com:631/ipp/print/EMEA_SecurePrint_IPP \
  -m everywhere -E

%preun redhat-printers
if [ $1 -eq 0 ]; then
  lpadmin -x RedHat_SecurePrint
fi

%files redhat-printers


%package redhat
Summary:        omos's package with internal RH configuration

Requires:       omos-repos-redhat
Requires:       omos-redhat-vpn
Requires:       omos-redhat-printers

Requires:       krb5-workstation
Requires:       rhpkg
Requires:       brewkoji
Requires:       qa-tools-workstation-1minutetip
Requires:       qa-tools-workstation-integration_scripts
Requires:       beaker-redhat conserver-client
Requires:       tmt-redhat-provision-minute
Requires:       tmt-redhat-provision-beaker

Requires:       python3-bugzilla python3-jira

# Lunch Bot:
Requires:       python3-irc

Requires:       revumatic

Requires:       openarena

Requires(post): ca-certificates

%description redhat
omos's package with internal RH configuration and packages.

%files redhat
%{_sysconfdir}/ipa/ca.crt
%{_sysconfdir}/krb5.conf.d/ipa_redhat_com
%{_libexecdir}/platform-python


%changelog
%autochangelog
